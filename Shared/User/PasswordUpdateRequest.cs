﻿namespace WebAuthnExample.Shared.User;

using System.ComponentModel.DataAnnotations;
using Services;

public class PasswordUpdateRequest
{
    [UsernameValidator]
    [Required]
    public string Username { get; set; } = null!;

    [Required]
    [DataType(DataType.Password)]
    public string CurrentPassword { get; set; } = null!;

    [Required]
    [PasswordValidator]
    [DataType(DataType.Password)]
    public string NewPassword { get; set; } = null!;
}

﻿namespace WebAuthnExample.Shared.User;

using System.ComponentModel.DataAnnotations;
using Services;

public class LoginRequest
{
    [Required]
    [UsernameValidator]
    public string Username { get; set; } = null!;

    [Required]
    [PasswordValidator]
    [DataType(DataType.Password)]
    public string Password { get; set; } = null!;
}

﻿namespace WebAuthnExample.Shared.User;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Services;

public class RegistrationRequest
{
    [UsernameValidator]
    [Required]
    public string Username { get; set; } = null!;
    
    [PasswordValidator]
    [DataType(DataType.Password)]
    [Required]
    public string Password { get; set; } = null!;
    
    public bool AutoLogin { get; set; } = true;
}

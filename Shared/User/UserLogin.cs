﻿namespace WebAuthnExample.Shared.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public record UserLogin(string Username, string BearerToken, bool Persistent)
{
}

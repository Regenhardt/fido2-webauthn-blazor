﻿console.log("Initializing webauthn.ts");

export function isWebAuthnPossible() {
    return !!window.PublicKeyCredential;
}

function toBase64Url(arrayBuffer: ArrayBuffer): string {
    return btoa(String.fromCharCode(...new Uint8Array(arrayBuffer))).replace(/\+/g, "-").replace(/\//g, "_").replace(/=*$/g, "");
}
function fromBase64Url(value: string): Uint8Array {
    return Uint8Array.from(atob(value.replace(/-/g, "+").replace(/_/g, "/")), c => c.charCodeAt(0));
}
function base64StringToUrl(base64String: string): string {
    return base64String.replace(/\+/g, "-").replace(/\//g, "_").replace(/=*$/g, "");
}

export async function createCreds(options: PublicKeyCredentialCreationOptions) {
    console.log('CreateOptions: ', options);
    if(typeof options.challenge === 'string')
        options.challenge = fromBase64Url(options.challenge);
    if (typeof options.user.id === 'string')
        options.user.id = fromBase64Url(options.user.id);
    if (options.rp.id === null)
        options.rp.id = undefined;
    console.log('Cleaned CreateOptions: ', options);
    var newCreds = await navigator.credentials.create({ publicKey: options }) as PublicKeyCredential;
    console.log("Created new credentials:", newCreds);
    const response = newCreds.response as AuthenticatorAttestationResponse;
    const retval = {
        id: base64StringToUrl(newCreds.id),
        rawId: toBase64Url(newCreds.rawId),
        type: newCreds.type,
        extensions: newCreds.getClientExtensionResults(),
        response: {
            attestationObject: toBase64Url(response.attestationObject),
            clientDataJSON: toBase64Url(response.clientDataJSON)
        }
    };
    console.log("Response:", retval);
    return retval;
}

export async function verify(options: PublicKeyCredentialRequestOptions) {
    console.log("Verifying creds with request:", options);
    if (typeof options.challenge === 'string')
        options.challenge = fromBase64Url(options.challenge);
    if (options.allowCredentials) {
        for (var i = 0; i < options.allowCredentials.length; i++) {
            const id = options.allowCredentials[i].id;
            if (typeof id === 'string')
                options.allowCredentials[i].id = fromBase64Url(id);
        }
    }
    console.log("Cleaned request:", options);
    var creds = await navigator.credentials.get({ publicKey: options }) as PublicKeyCredential;
    console.log("Creds:", creds);
    const response = creds.response as AuthenticatorAssertionResponse;
    const retval = {
        id: creds.id,
        rawId: toBase64Url(creds.rawId),
        type: creds.type,
        response: {
            authenticatorData: toBase64Url(response.authenticatorData),
            clientDataJSON: toBase64Url(response.clientDataJSON),
            userHandle: response.userHandle && response.userHandle.byteLength > 0 ? toBase64Url(response.userHandle) : undefined,
            signature: toBase64Url(response.signature)
        }
    }
    return retval;
}

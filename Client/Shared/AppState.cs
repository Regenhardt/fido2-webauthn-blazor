﻿namespace WebAuthnExample.Client.Shared;

using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using WebAuthnExample.Shared.User;
using WebAuthnExample.Shared.User.Services;

public class AppState
{
    public static string BuildDate { get; } = Assembly.GetExecutingAssembly().GetBuildDate();

    private readonly HttpClient httpClient;
    private readonly LocalStorage localStorage;
    private UserLogin? currentUser;
    public event Action? OnLoginStatusChanged;

    /// <summary>
    /// Currently logged in user.
    /// null means we're logged out.
    /// </summary>
    public UserLogin? CurrentUser
    {
        get => currentUser;
        set
        {
            currentUser = value;
            Task.Run(async () =>
            {
                if (currentUser is null)
                {
                    httpClient.DefaultRequestHeaders.Authorization = null;
                    await localStorage.Remove(UserDataValidator.TokenKey);
                }
                else
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", currentUser.BearerToken);
                    if(currentUser.Persistent)
                        await localStorage.Set(UserDataValidator.TokenKey, currentUser.BearerToken);
                }
                OnLoginStatusChanged?.Invoke();
            });
        }
    }

    public AppState(HttpClient httpClient, LocalStorage localStorage)
    {
        this.httpClient = httpClient;
        this.localStorage = localStorage;
    }
}

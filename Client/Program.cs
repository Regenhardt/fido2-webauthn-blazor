using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using RestEase;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using WebAuthnExample.Client;
using WebAuthnExample.Client.Shared;
using WebAuthnExample.Client.User;
using WebAuthn;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services
    .AddSingleton<LocalStorage>()
    .AddSingleton(_ => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) })
    .AddSingleton<WebAuthn.WebAuthn>()
    .AddSingleton<AppState>()
    .AddRestClientFor<IUserApi>();

await builder.Build().RunAsync();


internal static class Extensions
{
    private static readonly JsonSerializerOptions SerializerOptions = GetOptions();

    private static JsonSerializerOptions GetOptions()
    {
        var options = new JsonSerializerOptions
        {
            DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
        };
        options.AddContext<FidoBlazorSerializerContext>();
        return options;
    }
    private static readonly StjSerializer Serializer = new(SerializerOptions);
    private static readonly StjDeserializer Deserializer = new(SerializerOptions);

    internal static IServiceCollection AddRestClientFor<TInterface>(this IServiceCollection services)
        where TInterface : class =>
        services.AddSingleton(sp => new RestClient(sp.GetRequiredService<HttpClient>())
        {
            RequestBodySerializer = Serializer,
            ResponseDeserializer = Deserializer
        }.For<TInterface>());

    internal static string GetBuildDate(this Assembly assembly)
    {
        const string buildVersionMetadataPrefix = "+build";
        var attribute = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();
        if (attribute?.InformationalVersion != null)
        {
            var value = attribute.InformationalVersion;
            var index = value.IndexOf(buildVersionMetadataPrefix, StringComparison.Ordinal);
            if (index > 0)
            {
                return value[(index + buildVersionMetadataPrefix.Length)..];
            }
        }

        return "<Error in GetBuildDate>";
    }
}
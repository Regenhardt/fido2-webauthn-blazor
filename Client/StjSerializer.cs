﻿namespace WebAuthnExample.Client;

using System.Net.Http.Headers;
using System.Text.Json;
using RestEase;

public class StjSerializer : RequestBodySerializer
{
    private readonly JsonSerializerOptions options;

    public StjSerializer(JsonSerializerOptions? options = null)
    {
        this.options = options ?? JsonSerializerOptions.Default;
    }

    public override HttpContent? SerializeBody<T>(T body, RequestBodySerializerInfo info)
    {
        if (body == null)
            return null;

        var content = new StringContent(JsonSerializer.Serialize(body, options));

        const string contentType = "application/json";
        if (content.Headers.ContentType == null)
        {
            content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
        }
        else
        {
            content.Headers.ContentType.MediaType = contentType;
        }
        return content;
    }
}

public class StjDeserializer : ResponseDeserializer
{
    private readonly JsonSerializerOptions options;

    public StjDeserializer(JsonSerializerOptions? options = null)
    {
        this.options = options ?? JsonSerializerOptions.Default;
    }

    public override T Deserialize<T>(string? content, HttpResponseMessage response, ResponseDeserializerInfo info)
    {
        return JsonSerializer.Deserialize<T>(content!, options)!;
    }
}
﻿namespace WebAuthnExample.Client.User;

using Fido2NetLib;
using RestEase;

[BasePath("api/user")]
public interface IUserApi
{
    [Put("{username}/create-options")]
    Task<CredentialCreateOptions> GetCreationOptions([Path] string username);

    [Post("{username}/store-credential")]
    Task StoreCredential([Path] string username, [Body] AuthenticatorAttestationRawResponse credential);

    [Get("{username}/login-options")]
    Task<AssertionOptions> GetVerificationOptions([Path] string username);

    [Post("{username}/login")]
    Task<string> VerifyCredential([Path] string username, [Body] AuthenticatorAssertionRawResponse authVerification);
}

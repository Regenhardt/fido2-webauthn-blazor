﻿namespace WebAuthnExample.Server;

using Fido2NetLib;

public class FidoFactory
{
    private readonly Fido2Configuration defaultConfig;
    public FidoFactory(Fido2Configuration defaultConfig)
    {
        this.defaultConfig = defaultConfig;
    }

    /// <summary>
    /// Creates a new Fido2 instance for the given host.
    /// </summary>
    /// <param name="host"></param>
    /// <returns></returns>
    public IFido2 ForHost(string host)
    {
        // clean host
        host = host.Replace("https://", "").Replace("http://", "");
        var config = new Fido2Configuration
        {
            ServerDomain = host,
            ServerName = "WebAuthnExample",
            Origins = new HashSet<string>(defaultConfig.Origins),
            TimestampDriftTolerance = defaultConfig.TimestampDriftTolerance
        };
        return new Fido2(config);
    }
}

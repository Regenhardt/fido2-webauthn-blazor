﻿namespace WebAuthnExample.Server.Controllers;

using System.Text;
using Fido2NetLib;
using Fido2NetLib.Development;
using Fido2NetLib.Objects;
using Microsoft.AspNetCore.Mvc;

[Route("api/[controller]")]
[ApiController]
public class UserController : ControllerBase
{
    private readonly FidoFactory fido;

    // ------ For demo purposes only, this should be in a database. ------
    // For registration:
    private static CredentialCreateOptions lastCreationOptions = null!; // Store per user; temporarily until creation is finished.
    // For login:
    private static AssertionOptions lastAssertionOptions = null!; // Store per user; temporarily until login is finished.

    // One per user, per device.
    private static readonly Dictionary<string, AttestationVerificationSuccess> LastCreds = new(); // Store per user, permanently until they unregister a device.

    private static readonly Dictionary<string, StoredCredential> CredentialStore = new();
    // ------------------------------------------------------------------

    public UserController(FidoFactory fido)
    {
        this.fido = fido;
    }

    /// <summary>
    /// Gets a new challenge for the user to create a new credential.
    /// </summary>
    /// <remarks>
    /// https://itnext.io/biometrics-fingerprint-auth-in-your-web-apps-d5599522d0b3: "generate-and-store-challenge"
    /// Should persist the challenge mapped to the user.
    /// </remarks>
    /// <param name="username">User to create a new credential for.</param>
    /// <param name="relyingParty">Relying party for the credential, usually the domain of the webpage or the package of the app.</param>
    /// <returns>Options to pass to the browser.</returns>
    [HttpPut("{username}/create-options")]
    public CredentialCreateOptions GetCreationOptions([FromRoute] string username, [FromQuery(Name = "rpid")] string? relyingParty = null)
    {
        var opts = new CredentialCreateOptions
        {
            User = new Fido2User
            {
                DisplayName = username,
                Name = username.Replace(" ", ""),
                Id = Encoding.UTF8.GetBytes(username.Replace(" ", ""))
            },
            Challenge = Guid.NewGuid().ToByteArray(),
            Attestation = AttestationConveyancePreference.None,
            AuthenticatorSelection = AuthenticatorSelection.Default,
            Rp = new PublicKeyCredentialRpEntity(relyingParty ?? HttpContext.Request.Host.Host, "WebAuthnExample"), // On creation, ID will be autofilled to the current domain.
            ExcludeCredentials = new List<PublicKeyCredentialDescriptor>(0),
            PubKeyCredParams = new List<PubKeyCredParam>(0)
        };

        

        lastCreationOptions = opts;
        return opts;
    }

    /// <summary>
    /// Stores the created credential.
    /// </summary>
    /// <remarks>
    /// https://itnext.io/biometrics-fingerprint-auth-in-your-web-apps-d5599522d0b3: validate-and-store-public-key.
    /// </remarks>
    /// <param name="username">The user whose credential was created.</param>
    /// <param name="credential">The credential created by the user's device.</param>
    [HttpPost("{username}/store-credential")]
    public async Task CreateCredential([FromRoute] string username,
        [FromBody] AuthenticatorAttestationRawResponse credential)
    {
        var fido2 = fido.ForHost(lastCreationOptions.Rp.Id ?? HttpContext.Request.Host.Host);
        var credResult = await fido2.MakeNewCredentialAsync(credential, lastCreationOptions, IsCredentialUnique);
        if (credResult.Status != "ok" || credResult.Result == null)
            throw new Exception(credResult.ErrorMessage ?? "Unknown error");
        
        LastCreds[username] = credResult.Result;
        CredentialStore[username] = new StoredCredential
        {
            PublicKey = credResult.Result.PublicKey,
            SignatureCounter = credResult.Result.Counter,
            UserHandle = credResult.Result.User.Id,
            Descriptor = new PublicKeyCredentialDescriptor(credResult.Result.CredentialId),
            CredType = credResult.Result.CredType,
            RegDate = DateTime.Now,
            AaGuid = credResult.Result.Aaguid,
            UserId = credResult.Result.User.Id
        };
    }

    /// <summary>
    /// Gets a new challenge for the user to verify their credential.
    /// </summary>
    /// <param name="username">The user trying to login</param>
    /// <returns>New AssertionOptions for the user. Credential descriptor is blank if this user hasn't registered here yet.</returns>
    [HttpGet("{username}/login-options")]
    public AssertionOptions GetVerificationOptions(string username)
    {
        var fido2 = fido.ForHost(HttpContext.Request.Host.Host);
        if (CredentialStore.TryGetValue(username, out var lastCred))
            return lastAssertionOptions = fido2.GetAssertionOptions(new[] { lastCred.Descriptor },
                UserVerificationRequirement.Preferred);

        return lastAssertionOptions = fido2.GetAssertionOptions(new List<PublicKeyCredentialDescriptor>(0), UserVerificationRequirement.Preferred);
    }

    private Task<bool> IsCredentialUnique(IsCredentialIdUniqueToUserParams credentialiduserparams, CancellationToken cancellationtoken)
    {
        return Task.FromResult(true);
    }

    /// <summary>
    /// Verifies the user's credential and provides a JWT to use for authorization.
    /// </summary>
    /// <param name="username">The user trying to log in</param>
    /// <param name="authVerification">Response of the authenticator used.</param>
    /// <returns>A new JWT to use for authorization.</returns>
    [HttpPost("{username}/login")]
    public async Task<string> VerifyCredential([FromRoute] string username, [FromBody] AuthenticatorAssertionRawResponse authVerification)
    {
        var fido2 = fido.ForHost(HttpContext.Request.Host.Host);
        var result = await fido2.MakeAssertionAsync(
            authVerification, 
            lastAssertionOptions, 
            CredentialStore[username].PublicKey,
            CredentialStore[username].SignatureCounter,
            (user, token) => Task.FromResult(user.CredentialId.SequenceEqual(CredentialStore[username].Descriptor.Id))
            );

        if (result.Status == "ok")
        {
            CredentialStore[username].SignatureCounter = result.Counter;
            return "I'm a JWT trust me";
        }
        else
            return "";
    }
}
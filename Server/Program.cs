
using System.Text.Json;
using System.Text.Json.Serialization;
using WebAuthnExample.Server;

typeof(JsonSerializerOptions)
    .GetField("_defaultIgnoreCondition",
    System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)!
    .SetValue(JsonSerializerOptions.Default, JsonIgnoreCondition.WhenWritingNull);

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// get allowed hosts for fido config from appsettings (value are semicolon separated) into a hashset
var allowedOrigins = new HashSet<string>(builder.Configuration["AllowedOrigins"]!.Split(';'));
builder.Services.AddFido2(options =>
{
    options.ServerDomain = "localhost";
    options.ServerName = "WebAuthnExample";
    options.Origins = allowedOrigins;
    options.TimestampDriftTolerance = (int)TimeSpan.FromMinutes(5).TotalMilliseconds;
});
builder.Services.AddScoped<FidoFactory>();

builder.Services.AddControllersWithViews()
    .AddJsonOptions(opts =>
    {
        opts.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
    });
builder.Services.AddRazorPages();

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new() { Title = "WebAuthnExample", Version = "v1" });
    c.SchemaGeneratorOptions.SupportNonNullableReferenceTypes = true;
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseWebAssemblyDebugging();
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebAuthnExample");
});

app.UseHttpsRedirection();

app.UseBlazorFrameworkFiles();

app.UseStaticFiles();

app.UseRouting();


app.MapRazorPages();
app.MapControllers();
app.MapFallbackToFile("index.html");

app.Run();

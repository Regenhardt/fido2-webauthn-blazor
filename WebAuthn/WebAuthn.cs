﻿namespace WebAuthn;

using System.Diagnostics.CodeAnalysis;
using Fido2NetLib;
using Microsoft.JSInterop;

public class WebAuthn
{
    private IJSObjectReference jsModule = null!;
    private readonly Task initializer;

    public WebAuthn(IJSRuntime js)
    {
        initializer = Task.Run(async () =>
            jsModule = await js.InvokeAsync<IJSObjectReference>("import", "./webauthn.js"));
    }

    /// <summary>
    /// Wait for this to make sure this module is initialized.
    /// </summary>
    /// <returns></returns>
    public async Task Init() => await initializer;

    /// <summary>
    /// Whether or not this browser supports WebAuthn.
    /// </summary>
    /// <returns></returns>
    public async Task<bool> IsWebAuthnPossible() => await jsModule.InvokeAsync<bool>("isWebAuthnPossible");

    /// <summary>
    /// Creates a new credential.
    /// </summary>
    /// <param name="options"></param>
    /// <returns></returns>
    public async Task<AuthenticatorAttestationRawResponse> CreateCreds(CredentialCreateOptions options) =>
        await jsModule.InvokeAsync<AuthenticatorAttestationRawResponse>("createCreds", options);

    /// <summary>
    /// Verifies a credential for login.
    /// </summary>
    /// <param name="options"></param>
    /// <returns></returns>
    public async Task<AuthenticatorAssertionRawResponse> Verify(AssertionOptions options) =>
        await jsModule.InvokeAsync<AuthenticatorAssertionRawResponse>("verify", options);
}